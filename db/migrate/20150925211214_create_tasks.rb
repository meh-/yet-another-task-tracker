class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :title
      t.text :description
      t.integer :estimate
      t.string :state
      t.references :assignee
      t.references :author
      t.references :project
      t.timestamps null: false
    end

    add_index :tasks, :state
  end
end
