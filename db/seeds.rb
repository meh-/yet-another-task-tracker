# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.delete_all
Project.delete_all
Task.delete_all

User.create(email: 'admin@example.com', password: '11223344', admin: true)
5.times do |i|
  User.create(email: "user#{i}@example.com", password: '11223344')
end

3.times do
  Project.create(
      title: Faker::Lorem.sentence(2, true),
      description: Faker::Lorem.paragraph(2),
      users: User.all.to_a
  )
end

task_states = Task.possible_states
tags = Faker::Lorem.words(6)

50.times do
  task = Task.create(
      title: Faker::Lorem.sentence(3, true),
      description: Faker::Hacker.say_something_smart,
      estimate: rand(10) + 1,
      project: Project.offset(rand(Project.all.length)).first,
      assignee: User.offset(rand(User.all.length)).first,
      author: User.offset(rand(User.all.length)).first,
      state: task_states.sample
  )
  3.times do
    task.tag_list.add(tags.sample)
    task.save
  end

end