$(document).ready(function() {
    init_tasks_board();
});
$(window).bind('page:change', function() {
    init_tasks_board();
});

function init_tasks_board() {
    $('.draggable-task').draggable({
        revert: "invalid",
        zIndex: 100,
        start: function () {
            $(this).data("origPosition", $(this).position());
        }
    });

    $('.droppable-column').droppable({
        accept: function (draggable) {
            var taskState = draggable.prop('id').split('-')[0];
            var targetState = $(this).prop('id');
            var allowedTargets = getAllowedStates(taskState);
            return allowedTargets.indexOf(targetState) > -1;
        },
        activeClass: "droppable-active",
        hoverClass: "droppable-hover",
        drop: function (event, ui) {
            var taskId = ui.draggable.prop('id').split('-')[1];
            var targetState = $(this).prop('id');
            $.post('/tasks/' + taskId + '/' + targetState);
        }
    });
}

function getAllowedStates(state) {
    var allowedTargets;
    switch (state) {
        case 'open':
            allowedTargets = ['in_progress', 'resolved', 'closed'];
            break;
        case 'in_progress':
            allowedTargets = ['open', 'resolved', 'closed'];
            break;
        case 'resolved':
            allowedTargets = ['closed', 'reopened'];
            break;
        case 'reopened':
            allowedTargets = ['in_progress', 'resolved', 'closed'];
            break;
        case 'closed':
            allowedTargets = ['reopened'];
            break;
        default:
            allowedTargets = [];
    }
    return allowedTargets;
}