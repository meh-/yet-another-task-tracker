class Task < ActiveRecord::Base

  belongs_to :project
  belongs_to :author, class_name: 'User'
  belongs_to :assignee, class_name: 'User'

  validates_presence_of :title, :author, :description, :state

  after_initialize :set_initial_state

  acts_as_taggable

  default_scope -> { order('updated_at DESC') }

  delegate :email, to: :author, prefix: true
  delegate :email, to: :assignee, prefix: true

  state_machine :state, initial: :open do
    # ordering states for task_board
    state :open
    state :in_progress
    state :resolved
    state :reopened
    state :closed

    event :start_progress do
      transition [:open, :reopened] => :in_progress
    end

    event :stop_progress do
      transition :in_progress => :open
    end

    event :resolve do
      transition [:open, :in_progress, :reopened] => :resolved
    end

    event :close do
      transition [:open, :resolved, :reopened, :in_progress] => :closed
    end

    event :reopen do
      transition [:closed, :resolved] => :reopened
    end

  end

  def self.possible_states
    state_machine.states.map(&:name)
  end

  def transition_for_target_state(target_state)
    self.state_transitions.find { |transition| transition.to == target_state }
  end

  private
  def set_initial_state
    self.state ||= :open
  end
end
