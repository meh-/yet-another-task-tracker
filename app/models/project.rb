class Project < ActiveRecord::Base

  has_and_belongs_to_many :users
  has_many :tasks

  validates_presence_of :title, :description

  def total_members
    users.length
  end

  def total_tasks
    tasks.length
  end

  def all_tasks_by_state
    Task.possible_states.map { |state| [state, self.tasks.with_state(state)] }.to_h
  end

  def find_tasks_by_states(*states)
    states.map { |state| [state, self.tasks.with_state(state)] }.to_h
  end
end
