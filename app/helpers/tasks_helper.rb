module TasksHelper
  include ActsAsTaggableOn::TagsHelper
  def task_assignee(task)
    task.assignee ? task.assignee_email : 'Not assigned'
  end

  def users_for_assign(project)
    project.users.collect { |user| [user.email, user.id] }
  end
end
