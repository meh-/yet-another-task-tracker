module ProjectsHelper

  def members_for_project(project)
    project.users.present? ? project.users.collect(&:email) : ['there are no members']
  end
end
