class TasksController < ApplicationController
  before_action :set_project, only: [:index, :board, :edit, :new, :create, :destroy]
  before_action :set_task, only: [:show, :edit, :update, :destroy, :update_state, :fire_event]
  before_action :correct_user
  before_action :tag_cloud, only: [:board, :index]

  respond_to :js, only: [:update_state]

  def index
    @tasks = @project.tasks
    if params[:tag].present?
      @tasks = @tasks.tagged_with(params[:tag])
    end
    @tasks = @tasks.page params[:page]
  end

  def show
  end

  def new
    @task = Task.new
  end

  def edit
  end

  def create
    @task = Task.new(create_task_params)
    if @task.save
      redirect_to [@task.project, @task], notice: 'Task was successfully created'
    else
      render :new
    end
  end

  def update
    if @task.update(task_params)
      redirect_to [@task.project, @task], notice: 'Task was successfully updated'
    else
      render :edit
    end
  end

  def destroy
    @task.destroy
    redirect_to project_tasks_path(@project), notice: 'Task was successfully destroyed'
  end

  def board
    @tasks_by_states = @project.all_tasks_by_state
  end

  def update_state
    @tasks_by_states = @task.project.find_tasks_by_states @task.state_name, params[:state]

    transition = @task.transition_for_target_state(params[:state])
    if transition && @task.fire_state_event(transition.event)
      flash.now[:success] = "Task '#{@task.title}' is moved to state: #{@task.human_state_name}"
    end
  end

  def fire_event
    @task.fire_state_event(params[:event])
    redirect_to [@task.project, @task]
  end

  private
  def tag_cloud
    @tags = Task.tag_counts_on :tags
  end

  private

  def set_project
    @project = Project.find(params[:project_id])
    redirect_to root_path unless @project.present?
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_task
    @task = Task.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def task_params
    params.require(:task).permit(:estimate, :title, :description, :assignee_id, :tag_list)
  end

  def create_task_params
    task_params.merge(author: current_user, project: @project)
  end

  def correct_user
    redirect_to root_path unless current_user.admin? || current_user.projects.include?(@project)
  end
end
